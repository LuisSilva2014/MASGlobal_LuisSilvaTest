﻿//Seteamos la localización
var getLocalization = function () {
    var localizationobj = {};
    localizationobj.pagerGoToPageString = "Ir a página:";
    localizationobj.pagerShowRowsString = "Mostrar Registros:";
    localizationobj.pagerRangeString = " De ";
    localizationobj.pagerNextButtonString = "Siguiente";
    localizationobj.pagerFirstButtonString = "Primero";
    localizationobj.pagerLastButtonString = "Ultimo";
    localizationobj.pagerPreviousButtonString = "Anterior";
    localizationobj.sortAscendingString = "Orden Ascendente";
    localizationobj.sortDescendingString = "Orden Descendente";
    localizationobj.sortRemoveString = "Remover orden";
    localizationobj.firstDay = 1;
    localizationobj.percentSymbol = "%";
    localizationobj.currencySymbol = "€";
    localizationobj.currencySymbolPosition = "despues";
    localizationobj.decimalSeparator = ".";
    localizationobj.thousandsSeparator = ",";
    localizationobj.filterselectstring = "Buscar...";
    localizationobj.filterstring = "Buscar";
    localizationobj.filterclearstring = "Limpiar";
    localizationobj.loadtext = "Cargando";
    localizationobj.emptydatastring = "No hay datos para mostrar",
    localizationobj.todaystring = "Hoy";
    localizationobj.filterClearString = "Limpiar filtros";
    localizationobj.filterSearchString = "Buscar por nombre";
    var days = {
        // full day names
        names: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        // abbreviated day names
        namesAbbr: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        // shortest day names
        namesShort: ["D", "L", "M", "X", "J", "V", "S"]
    };
    localizationobj.days = days;
    var months = {
        // full month names (13 months for lunar calendards -- 13th month should be "" if not lunar)
        names: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Novienbre", "Diciembre", ""],
        // abbreviated month names
        namesAbbr: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic", ""]
    };
    var patterns = {
        d: "dd.MM.yyyy",
        D: "dddd, d. MMMM yyyy",
        t: "HH:mm",
        T: "HH:mm:ss",
        f: "dddd, d. MMMM yyyy HH:mm",
        F: "dddd, d. MMMM yyyy HH:mm:ss",
        M: "dd MMMM",
        Y: "MMMM yyyy"
    }
    localizationobj.patterns = patterns;
    localizationobj.months = months;
    return localizationobj;
}