﻿using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
namespace MASGlobalTest_LuisSilva
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            // Web API routes
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();


            //Inicializador del swagger  EN CASO DE NO EXISTIR una vista inical
            config.Routes.MapHttpRoute(
            //name: "Swagger UI",
            name: "Apis BackEnd Centralizer",
            routeTemplate: "",
            defaults: null,
            constraints: null,
            handler: new RedirectHandler(message => message.RequestUri.ToString().TrimEnd('/'), "swagger/ui/index"));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}
