﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL;
using DTOLayer;
namespace MASGlobalTest_LuisSilva.Controllers
{
    public class annualSalaryController : ApiController
    {
        /// <summary>
        /// Get annual salary
        /// </summary>
        /// <param name="ID">ID</param>
        /// <returns>Return a string</returns>
        public string get(string ID)
        {
            return new employeeHander().getAnualSalary(ID);
        }

    }
}
