import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

// import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
// tslint:disable-next-line:max-line-length
// import {MdButtonModule, MdCardModule,MdMenuModule,MdToolbarModule,MdIconModule, MdCheckboxModule,OverlayContainer} from '@angular/material';

// import { DataTablesModule } from 'angular-datatables';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

// ------- Componentes para los mdulos ----
import { EmployeeComponent } from './modules/employee/employee.component';

// ------- Services ----
import { FormsModule } from '@angular/forms';
import { EmployeeService } from './services/employee/employee.service';
import { AnnualSalaryService } from './services/annualsalary/annualsalary.service';
import { LoaderService } from './services/loader/loader.service';
// ------- Navegation componets ----
import { Error404Component } from './navigation/error404/error404.component';
import { NavFooterComponent } from './navigation/nav-footer/nav-footer.component';
import { NavContainerComponent } from './navigation/nav-container/nav-container.component';
import { NavSidebarLeftComponent } from './navigation/nav-sidebar-left/nav-sidebar-left.component';
import { NavSidebarTopComponent } from './navigation/nav-sidebar-top/nav-sidebar-top.component';
import { LoginComponent } from './modules/login/login.component';
import { LoginService } from './services/login/login.service';

// import { Ng2SmartTableModule } from 'ng2-smart-table';

const routesApp: Routes = [
  { path: 'login' , component: LoginComponent },
  { path: 'employees' , component: EmployeeService },
  { path: '404' , component: Error404Component },
  { path: '' , redirectTo: '' , pathMatch: 'full' }, 
  { path: '**' , redirectTo: '404' }
];

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    // AnnualSalaryComponent,
    NavSidebarLeftComponent,
    NavSidebarTopComponent,
    Error404Component,
    NavFooterComponent,
    NavContainerComponent,
    LoginComponent
  ],
  imports: [
    RouterModule.forRoot(routesApp),  // This line contains the routes
    BrowserModule,
    HttpModule,
    FormsModule, /*To resolve this issue: Can't bind to 'ngModel' since it isn't a known property of 'input'    */
    // Ng2SmartTableModule
    // DataTablesModule
  ],
  providers: [
    LoaderService, // In this section you should define the loader to be avalaible for the whole aplicaction
    EmployeeService, // In this area, are the shared services
    AnnualSalaryService,
    LoginService,
    LoginComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

