import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map'; // Realizar el mapeo a json
import swal from 'sweetalert2';

declare var jquery: any; // Importamos jquery
declare var $: any;
declare var GlobalTools: any;
import '../../../assets/contentlocal/js/GlobalTools.js';
import { AnnualSalaryService } from '../../services/annualsalary/annualsalary.service';
import { EmployeeService } from '../../services/employee/employee.service';
import { LoaderService } from '../../services/loader/loader.service';

class EmployeesDto {
  ID: number;
  fullName: string;
  typeContract: string;
  rate: string;
  dateCreation: string;
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  Model: any = {};
  ErrorRequest: any = 'Server not available';
  ModelEdit: any = {};
  Employees: EmployeesDto[] = [];
  errorHttp: Boolean;
  loadingLocal: Boolean = true;
  UrlApi: any;
  OldIDEditLocal: any;
  ItemSelected: any;
  headerss = new Headers({ 'Content-Type': 'application/json' });

  TypeContract = [
    { id: "Monthly", name: 'Monthly' },
    { id: "Hourly", name: 'Hourly' }
  ];
  
  selectedValue = null;
  Result = null;

  // settings = {
  //   columns: {
  //     ID: {
  //       title: 'ID'
  //     },
  //     fullName: {
  //       title: 'Full Name'
  //     },
  //     typeContract: {
  //       title: 'typeContract'
  //     },
  //     rate: {
  //       title: 'rate'
  //     }
  //     ,
  //     dateCreation: {
  //       title: 'dateCreation'
  //     }
  //   }
  // };

  constructor(private EmployeeService: EmployeeService, private AnnualSalaryService: AnnualSalaryService,
              private LoaderService: LoaderService
              ) { }


  ngOnInit(): void {
    $(document).ready(function () {
      $('.modal').modal();
    });
    this.fillDataTable();
  }


  public fillDataTable() {
    this.showLoading();
     this.EmployeeService.getAll().subscribe((data) => {
        this.Employees = data;
        this.OcultarLoading();
    },
    error => {
      this.hideLoadingAndContent();
    });
  }

  public edition(i) {
    $('#ModalEdit').modal('open');
    this.ItemSelected = false;
    this.OldIDEditLocal = this.Employees[i].ID;
    this.ModelEdit.ID = this.Employees[i].ID;
    this.ModelEdit.fullName = this.Employees[i].fullName;
    this.ModelEdit.rate = this.Employees[i].rate;
    this.ModelEdit.typeContract = this.Employees[i].typeContract;
    this.ItemSelected = i;
  }

  public getAnnualSalary(i) {
    this.showLoading();
     this.AnnualSalaryService.get().subscribe((data) => {
        // this.Employees = data;
        this.OcultarLoading();
    },
    error => {
      this.hideLoadingAndContent();
    });
  }

  public saveEdition() {
      this.showLoading();
       // Request the api
      this.EmployeeService.put(this.NameOldEditLocal, this.ModelEdit).subscribe(result => {
        if (result === 'Update') {
          // Client action
          const i: any = this.ItemSelected;
          for (let j = 0; j < this.Employees.length; j++) {
            if (i === j) {
              this.Employees[i] = this.ModelEdit;
              GlobalTools.CargarMensajeSA_2_Simple('Record updated succesfull', '', 'success', 'Acept', '', '', '', 0);
              this.ModelEdit = {};
              this.OcultarLoading();
            }
          }
        }
      },
      error => {
         this.OcultarLoading();
          GlobalTools.CargarMensajeSA_2_Simple(this.ErrorRequest, '', 'error', 'Acept', '', '', '', 0);
      });
  }

  public delete(NameObj, i) {
    swal(
      {
        title: '¿Are you sure?',
        text: 'Once you deleted you can not reverse this action',
        type: 'info',
        showCancelButton: true,
        cancelButtonText: 'No',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }
    ).then(() => {
        this.showLoading();
        // Accion en el servidor
        this.EmployeeService.delete(NameObj).subscribe(result => {
          if (result === 'Deleted success') {
            GlobalTools.CargarMensajeSA_2_Simple('This row had been deleted', '', 'success', 'Acept', '', '', '', 0);
            // Client action
            this.OcultarLoading();
            this.Employees.splice(i, 1);
          }
        },
        error => {
            this.OcultarLoading();
            GlobalTools.CargarMensajeSA_2_Simple(this.ErrorRequest, '', 'error', 'Acept', '', '', '', 0);
        });
    });
  }

  public post() {
    this.showLoading();
    this.EmployeeService.post(this.Model).subscribe(result => {
        if (result === 'New employee') {
          GlobalTools.CargarMensajeSA_2_Simple('Record added!', '', 'success', 'Acept', '', '', '');
         // Client action
          this.Employees.push(this.Model);
          this.OcultarLoading();
        }
    },
    error => {
        this.OcultarLoading();
        GlobalTools.CargarMensajeSA_2_Simple(this.ErrorRequest, '', 'error', 'Acept', '', '', '', 0);
    });
  }

  // ------------- Loading section  --------
  showLoading() {
    this.loadingLocalFn(true, true);
  }
  OcultarLoading() {
    this.loadingLocalFn(false, true);
  }
  hideLoadingAndContent() {
    this.loadingLocalFn(false, false);
  }
  public loadingLocalFn(loader: boolean, conten: boolean) {
     this.LoaderService.loadContent(loader, conten);
  }
  // ------------- End  --------
}