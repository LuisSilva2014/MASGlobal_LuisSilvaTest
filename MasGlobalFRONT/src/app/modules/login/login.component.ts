import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map'; // Realizar el mapeo a json
import swal from 'sweetalert2';

declare var jquery: any; // Importamos jquery
declare var $: any;
declare var GlobalTools: any;
import '../../../assets/contentlocal/js/GlobalTools.js';
import { LoginService } from '../../services/login/login.service';
import { LoaderService } from '../../services/loader/loader.service';

class LoginDto {
  User: string;
  Pass: string;
  rechapcha: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Model: any = {};
  ErrorRequest: any = 'Server not available';
  Login: LoginDto[] = [];
  errorHttp: Boolean;
  cargandoLocal: Boolean = true;
  headerss = new Headers({ 'Content-Type': 'application/json' });
  timer: any;

  constructor(private LoginService: LoginService,
    private LoaderService: LoaderService
  ) { }

  ngOnInit(): void {

  }

  public loginP() {
    console.log(this.Model.User);
    if (this.Model.User === ''  ||  this.Model.User === undefined ) {
      GlobalTools.CargarMensajeSA_2_Simple('Please, enter your password', '', 'warning', 'Acept', '', '', '');
    } else if (this.Model.Pass  === ''  ||  this.Model.Pass === undefined) {
      GlobalTools.CargarMensajeSA_2_Simple('Please, enter your password', '', 'warning', 'Acept', '', '', '');
    } else {

      this.MostrarLoading();
      this.LoginService.loginService(this.Model).subscribe(result => {
        this.OcultarLoading();

        if (result === true) {
          localStorage.setItem('UserAccess', 'true');
          this.LoaderService.LoginOn(true);

          this.timer = setTimeout(() => {   //  setTimeout(function ()  in angular
              GlobalTools.CargarMensajeSA_2_Simple('Welcome manager', '', 'success', 'Acept', '', '', '');
          }, 1000);

        } else {
          GlobalTools.CargarMensajeSA_2_Simple('The credentials are invalid', '', 'error', 'Acept', '', '', '');
        }
      },
        error => {
          this.OcultarLoading();
          GlobalTools.CargarMensajeSA_2_Simple(this.ErrorRequest, '', 'error', 'Acept', '', '', '', 0);
        });
    }
  }

  // ------------- LOADING REGION  --------
  MostrarLoading() {
    this.cargandoLocalFn(true, true);
  }
  OcultarLoading() {
    this.cargandoLocalFn(false, true);
  }

  public cargandoLocalFn(loader: boolean, conten: boolean) {
    this.LoaderService.CargaContenido(loader, conten);
  }
}