import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LoaderService {
    public statusLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public statusContenido: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public loginOn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() { }
    loadContent (value: boolean, value2: boolean) {
        this.statusLoading.next(value);
        this.statusContenido.next(value2);
    }
    LoginOn (value: boolean) {
      this.loginOn.next(value);
  }
}