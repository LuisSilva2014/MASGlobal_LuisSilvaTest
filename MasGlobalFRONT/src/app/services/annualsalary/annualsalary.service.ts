import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class AnnualSalaryService {
  Model: any = {};
  headerss = new Headers({ 'Content-Type': 'application/json' });
  myParams = new URLSearchParams();

   UrlApi = 'http://luis-silva-temp3.azurewebsites.net/api/annualsalary';
  // UrlApi = 'http://localhost:23321/api/annualsalary';

  constructor(private http: Http) { }

  get() {
    return this.http.get(this.UrlApi).map(result => {
      return result.json();
   });
  }

}
