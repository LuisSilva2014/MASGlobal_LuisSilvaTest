import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSidebarTopComponent } from './nav-sidebar-top.component';

describe('NavSidebarTopComponent', () => {
  let component: NavSidebarTopComponent;
  let fixture: ComponentFixture<NavSidebarTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSidebarTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSidebarTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
